extends KinematicBody

export var speed = 5
var velocity = Vector3.ZERO

func _ready():
	$AnimationPlayer.play("Wheels")

func _physics_process(delta):
	var direction = Vector3.ZERO

	direction.x += Input.get_action_strength('move_right')
	direction.x -= Input.get_action_strength('move_left')
	direction.z += Input.get_action_strength('move_back')
	direction.z -= Input.get_action_strength('move_forward')

	if direction == Vector3.ZERO:
		$AnimationPlayer.playback_speed = 0
	else:
		var length = direction.length()
		$AnimationPlayer.playback_speed = min(length, 1.0)
		if length > 1.0:
			direction = direction.normalized()
		$Pivot.look_at(translation + direction, Vector3.UP)

	velocity.x = direction.x * speed
	velocity.z = direction.z * speed

	velocity = move_and_slide(velocity, Vector3.UP)
	
	if Input.is_action_just_pressed('helmet'):
		if $Pivot/Helmet.visible:
			$Pivot/Helmet.hide()
		else:
			$Pivot/Helmet.show()
